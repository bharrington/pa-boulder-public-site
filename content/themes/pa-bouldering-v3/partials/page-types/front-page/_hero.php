<section class="hero">
  <?php if( have_rows('home_hero_banner', 'option') ): $i = 0;?>
    <div class="hero__item">
      <?php while( have_rows('home_hero_banner', 'option') ): the_row(); $i++;
        $hero_banner      = get_sub_field('image');
        $banner_resized_l = wp_get_attachment_image_src($hero_banner, 'hero-banner-l');
        $banner_resized_m = wp_get_attachment_image_src($hero_banner, 'hero-banner-m');
        $banner_resized_s = wp_get_attachment_image_src($hero_banner, 'hero-banner-s'); ?>

        <img class="hero__item-image" src="<?php echo $banner_resized_l['0']; ?>"
             srcset="<?php echo $banner_resized_l['0']; ?> 1024w, <?php echo $banner_resized_m['0']; ?> 640w, <?php echo $banner_resized_s['0']; ?> 320w"
             sizes="100vw" />
      <?php endwhile; ?>
    </div>
  <?php endif; ?>
</section>
