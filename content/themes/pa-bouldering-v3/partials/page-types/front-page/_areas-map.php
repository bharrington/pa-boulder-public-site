<div id="areas__map"></div>
<div class="expand-areas__map">expand</div>

<script>
function initAreasMap() {
  var locations = [

    <?php
      $areas_args = array(
        'posts_per_page' => -1,
        'post_type' => 'areas'
      );

      $counter = 0;

      $areas = new WP_Query( $areas_args );
    ?>
      <?php while ($areas->have_posts()) : $areas->the_post(); $counter++;?>

      <?php $location = get_field('boulder_gps'); ?>

      <?php if ( $location ): ?>
      ['<a class="map-link-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a><p class="areas__item-rating small"><?php the_field("area_rating"); ?></p>', <?php echo $location; ?>, <?php echo $counter ?>],
      <?php endif; ?>
      <?php endwhile; ?><?php wp_reset_query(); ?>
  ];

  var image = {
    url: 'http://pabouldering.com/content/uploads/new-icon.png',
    // This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(20, 32),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(0, 32)
  };

  window.map = new google.maps.Map(document.getElementById('areas__map'), {
    <?php $pa_global = 'partials/global/'; ?>
    <?php get_template_part( $pa_global.'_map-styles' ); ?>
  });

  var infowindow = new google.maps.InfoWindow();

  var bounds = new google.maps.LatLngBounds();

  for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map,
          icon: image,
      });

      bounds.extend(marker.position);

      google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
          }
      })(marker, i));
  }

  map.fitBounds(bounds);

  var listener = google.maps.event.addListener(map, "idle", function () {
      // map.setZoom(12);
      google.maps.event.removeListener(listener);
  });

  google.maps.event.addDomListener(window, 'resize', function() {
    map.fitBounds(bounds);
  });
}

function areasMapLoad() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD_RKDE9qEjk1SGYOZVhFC1hy0SaOQzyNw&' + 'callback=initAreasMap';
  document.body.appendChild(script);
}

window.onload = areasMapLoad;

</script>
