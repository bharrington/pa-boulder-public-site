<section class="spray spray--latest">
  <nav>
    <ul class="breadcrumb">
      <li class="breadcrumb__item breadcrumb__item-current">Hot Spray</li>
      <li class="breadcrumb__item"><a href="<?php if( get_option( 'show_on_front' ) == 'page' ) echo get_permalink( get_option('page_for_posts' ) ); else echo bloginfo('url');?>">Full Strength Spray <span>&rarr;<span></a></li>
    </ul>
  </nav>

  <?php
    $latest_spray = array(
      'posts_per_page' => 1,
      'post_type' => 'post',
    );

    $spray = new WP_Query( $latest_spray );
    while ($spray->have_posts()) : $spray->the_post();
  ?>

  <article class="spray__item">
    <date class="spray__item-date"><?php the_date('m.d.Y'); ?></date>
    <h3 class="spray__item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <?php if( get_field('post_teaser') ): ?>
      <p class="spray__item-teaser"><?php the_field('post_teaser'); ?></p>
    <?php endif; ?>
  </article>

  <?php endwhile; ?><?php wp_reset_query(); ?>
</section>
