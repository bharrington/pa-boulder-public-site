<div class="popup-trigger" data-popup>
  <h3 class="popup-trigger__title">Join the mailing list for the <br />PA Bouldering Guides updates.</h3>
</div>

<i class="popup-trigger__close" data-close-popup>Close</i>

<div class="remodal" data-remodal-id="modal">
  <button data-remodal-action="close" class="remodal-close"></button>

  <div class="popup__wrap">
    <p class="popup-text" data-form-success>
      Want to stay up to date on developments about the long awaited PA Bouldering Guidebook? <br / >Join the mailing list to stay up to date of the newness.
    </p>



<div class="popup-form" id="mc_embed_signup">
<form action="//pabouldering.us16.list-manage.com/subscribe/post?u=4f4b7821992b5723643b788f7&amp;id=19b4bebbd2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">

<style>
#mc_embed_signup div.mce_inline_error {
  background: white;
  font-weight: normal;
  color: black;
  font-size: 12px;
  line-height: 1.2;
}
</style>

<div class="mc-field-group">
  <input type="email" value="" placeholder="Email Address" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
  <div id="mce-responses" class="clear">
    <div class="response" id="mce-error-response" style="display:none"></div>
    <div class="response" id="mce-success-response" style="display:none"></div>
  </div>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4f4b7821992b5723643b788f7_19b4bebbd2" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='BIRTHDAY';ftypes[3]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
</div>

<!--End mc_embed_signup-->

  </div>
</div>

