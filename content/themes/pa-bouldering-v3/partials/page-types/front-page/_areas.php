<section class="areas">
<?php
  $areas_args = array(
    'posts_per_page' => -1,
    'post_type' => 'areas',
    'meta_query' => array(
      'rating' => array(
        'key' => 'area_rating',
      ),
      'problems' => array(
        'key' => 'area_problem_count',
      ),
    ),
    'orderby' => array(
      'rating' => 'DESC',
      'problems' => 'DESC',
    ),
  );

  $areas = new WP_Query( $areas_args );
?>

  <article>
    <ul class="areas__listing not-list">
      <?php while ($areas->have_posts()) : $areas->the_post();
        $rating    = get_field("area_rating");
        $problems  = get_field("area_problem_count");
        $area_dev  = get_field('area_active_dev');
      ?>
      <li class="areas__item">
        <div class="card">
        <a class"__animate" href="<?php the_permalink(); ?>">
          <h3 class="areas__item-name"><?php the_title(); ?></h3>

          <?php if( $rating ): ?>
            <p class="areas__item-rating"><?php the_field('area_rating'); ?></p>
          <?php endif; ?>

          <?php if( $problems ): ?>
            <p class="areas__item-problem-count"><?php the_field('area_problem_count'); ?><?php if( $area_dev ): ?>+<?php endif; ?> problems</p>
          <?php endif; ?>
        </a>
        <div>
      </li>
    <?php endwhile; ?><?php wp_reset_query(); ?>
    </ul>
  </article>
</section>
