<?php the_post();
  $rating    = get_field("area_rating");
  $problems  = get_field("area_problem_count");
  $town      = get_field("area_closest_town");
  $map       = get_field("area_map");
  $guide     = get_field("area_link_to_guide");
  $guide_v   = get_field("guide_version");
  $boulder   = get_field('boulder_gps');
  $parking   = get_field('parking_gps');
  $area_dev  = get_field('area_active_dev');
?>

<section class="areas__item areas__item--single">
  <aside id="areas__map--single" class="areas__item-map">
    <iframe src="<?php echo $map ?>" width="100%"></iframe>
  </aside>

  <article class="areas__item-content">
    <div class="area__item-wrap">
      <h3 class="areas__item-name"><?php the_title(); ?></h3>

      <?php if( $rating ): ?>
        <p class="areas__item-rating"><?php echo $rating; ?></p>
      <?php endif; ?>

      <?php if( $town ): ?>
        <p class="areas__item-location"><?php echo $town; ?></p>
      <?php endif; ?>

      <div class="area__item-about">
        <?php the_content(); ?>
      </div>

      <div class="area__item-assets">
        <?php if( $guide ): ?>
          <a class="btn" target="_blank" href="<?php echo $guide; ?>">
            Mini Guide v<?php echo $guide_v; ?>
          </a>
        <?php endif;  ?>

        <?php
          $id = get_the_ID();
          $args = array(
          'post_type'       => array( 'post' ),
          'posts_per_page'  => -1,
          'meta_query'      => array(
            array(
              'key'         => 'related_area',
              'value'       => $id,
              'compare'     => 'LIKE'
              )
            )
          );

          $related_posts = new WP_Query( $args );
        ?>
        <?php if ($related_posts->have_posts()) : ?>
        <div class="btn reveal-related-spray">Related Spray</div>
        <?php endif; wp_reset_query(); ?>
      </div>

      <div class="area__item-coordinates">
        <?php if( $problems ): ?>
          <div class="area__item-problem-count"><?php echo $problems; ?><?php if( $area_dev ): ?>+<?php endif; ?> problems</div>
        <?php endif; ?>

        <?php if( $parking ): ?>
          <div class="area__item-gps--parking"><?php echo $parking; ?></div>
        <?php endif; ?>

        <?php if( $boulder ): ?>
          <div class="area__item-gps--boulders"><?php echo $boulder; ?></div>
        <?php endif; ?>
      </div>
    </div>
  </article>
</section>
