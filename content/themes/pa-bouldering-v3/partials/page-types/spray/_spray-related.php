<?php
  $id = get_the_ID();
  $args = array(
  'post_type'       => array( 'post' ),
  'posts_per_page'  => -1,
  'orderby'         => 'DESC',
  'meta_query'      => array(
    array(
      'key'         => 'related_area',
      'value'       => $id,
      'compare'     => 'LIKE'
      )
    )
  );

  $related_posts = new WP_Query( $args );
?>
<?php if ($related_posts->have_posts()) : ?>

<section class="spray spray--listing spray--listing-related">
  <nav>
    <ul class="breadcrumb">
      <?php if( the_field('area_nickname') ): ?>
        <li class="breadcrumb__item breadcrumb__item-current"><?php the_field('area_nickname'); ?> Spray</li>
      <?php else: ?>
        <li class="breadcrumb__item breadcrumb__item-current"><?php the_title(); ?> Spray</li>
      <?php endif; ?>
    </ul>
  </nav>

  <div class="scrollable">
    <?php while ($related_posts->have_posts()) : $related_posts->the_post(); ?>

    <article class="spray__item">
      <date class="spray__item-date"><?php the_date('m.d.Y'); ?></date>
      <h3 class="spray__item-title"><a href="<?php the_permalink(); ?>" rel="<?php the_ID(); ?>" class="pull-post"><?php the_title(); ?></a></h3>
      <?php if( get_field('post_teaser') ): ?>
        <p class="spray__item-teaser"><?php the_field('post_teaser'); ?></p>
      <?php endif; ?>
    </article>

    <?php endwhile; ?><?php wp_reset_query(); ?>
  </div>
</section>

<?php endif; wp_reset_query(); ?>
