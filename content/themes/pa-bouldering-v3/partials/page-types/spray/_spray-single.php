<?php the_post(); ?>

<section class="spray spray--single">
  <nav>
    <ul class="breadcrumb">
      <li class="breadcrumb__item breadcrumb__item-current breadcrumb__item-close"><a href="javascript:history.back()"><span>&larr;</span> Back to Spray</a></li>
    </ul>
  </nav>

  <article class="spray__item">
    <date class="spray__item-date"><?php the_date('m.d.Y'); ?>
    <?php $related_area = get_field('related_area');
      if( $related_area ): $post = $related_area; setup_postdata( $post );
    ?>
      / <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <?php wp_reset_postdata(); endif; ?>
    </date>
    <h3 class="spray__item-title"><?php the_title(); ?></h3>

    <?php if( get_field('flexible_content_post') ):
      while ( has_sub_field('flexible_content_post') ) :
        $rowName = get_row_layout();
        echo '<div class="row '.$rowName.'">';
        include('layouts/'.$rowName.'.php');
        echo '</div>';
      endwhile;
    endif;?>

  <?php $disqus = get_field('disqus_id', 'options'); ?>
  <?php if( $disqus ): ?>
    <div class="disqus" id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = '<?php echo $disqus ?>';

        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
  <?php endif; ?>

  </article>
</section>
