<section class="spray spray--listing">
  <nav>
    <ul class="breadcrumb">
      <li class="breadcrumb__item breadcrumb__item-current">Full Strength Spray</li>
    </ul>
  </nav>

  <?php
  $args = array(
    'post_type'       => 'post',
    'posts_per_page'  => -1,
    'orderby'         => 'DESC',
  );

  $posts = new WP_Query( $args );
?>
<?php if ($posts->have_posts()) : ?>
<?php while ($posts->have_posts()) : $posts->the_post(); ?>

  <article class="spray__item">
    <date class="spray__item-date"><?php the_date('m.d.Y'); ?></date>
    <h3 class="spray__item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <?php if( get_field('post_teaser') ): ?>
      <p class="spray__item-teaser"><?php the_field('post_teaser'); ?></p>
    <?php endif; ?>
  </article>

  <?php endwhile; endif; wp_reset_query(); ?>
</section>
