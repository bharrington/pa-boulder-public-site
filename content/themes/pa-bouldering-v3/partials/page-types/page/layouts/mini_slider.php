<?php $flex_images = get_sub_field('mini_slider'); if( $flex_images ): ?>
<div class="mini-slider owl-carousel">
  <?php foreach( $flex_images as $flex_image ): ?>

    <?php // Slider Image
      $flex_gallery = $flex_image['id'];
      $gallery_image_resized = wp_get_attachment_image_src($flex_gallery, 'mini-slider');
    ?>
  <div class="item">
    <img src="<?php echo $gallery_image_resized[0]; ?>">
  </div>
  <?php endforeach; ?>
</div>
<?php endif; ?>
