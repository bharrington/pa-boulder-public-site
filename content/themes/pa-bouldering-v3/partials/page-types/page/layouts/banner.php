<?php
  $img  = get_sub_field('image');
  $size = 'hero';
  $img_resized = $img['sizes'][$size];
  $img_caption = $img['caption'];
?>

<div class="banner">
  <img class="banner__image" src="<?php echo $img_resized; ?>">
  <?php if ( $img_caption ): ?>
    <small class="banner__caption"><?php echo $img_caption; ?></small>
  <?php endif; ?>
</div>
