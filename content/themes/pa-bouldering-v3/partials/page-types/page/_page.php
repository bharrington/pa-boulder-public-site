<?php the_post(); ?>
<section class="page page--single">
  <article class="page__item">
    <h3 class="page__item-title"><?php the_title(); ?></h3>

    <?php if( get_field('flexible_content_post') ):
      while ( has_sub_field('flexible_content_post') ) :
        $rowName = get_row_layout();
        echo '<div class="row '.$rowName.'">';
        include('layouts/'.$rowName.'.php');
        echo '</div>';
      endwhile;
    endif;?>

  </article>
</section>
