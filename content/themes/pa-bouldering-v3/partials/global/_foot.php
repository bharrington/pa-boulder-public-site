<a href="javascript:void(0)" class="reveal-contact" data-reveal-contact>Contact</a>

<footer>
  <a href="javascript:void(0)" class="reveal-contact-close" data-reveal-contact>Contact</a>

  <div class="footer__inner">
  <h2>I would love to hear from you!</h2>
  <p>Send your gnar send videos, results of a weekend photo shoot, stories, rants, thoughts, beta corrections or just a friendly <em>Hello</em>. If you are interested in writing a blog post give a holler as well. Always on the look out for guest writers.</p>

  <p>If you ever need anything you can email at <a href="mailto:crushing@pabouldering.com">crushing@pabouldering.com</a></p>

  <p>You can also send join the <a href="http://pabouldering.com#modal">Mailing List</a> to stay informed when a new mini guide or a full print guide gets released.</p>
  </div>

</footer>

<?php wp_footer(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48113179-3', 'auto');
  ga('send', 'pageview');
</script>
</body>
</html>
