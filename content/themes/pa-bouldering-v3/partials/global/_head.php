<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<title><?php bloginfo('name'); ?> - <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >

<a class="logo" href="<?php echo home_url(); ?>">Northern PA Bouldering</a>

<?php if( !is_page( 'mini-guides' ) ): ?>
  <a class="__animation btn mini-guides__link" href="<?php echo home_url(); ?>/mini-guides/" title="Pennslyvania Bouldering Guides">Guides &nbsp;↓</a>
<?php endif; ?>

<?php if( !is_page( 'contribute' ) ): ?>
  <a class="__animation btn contribute__link" href="<?php echo home_url(); ?>/contribute/" title="Help Contribute">Contribute</a>
<?php endif; ?>
