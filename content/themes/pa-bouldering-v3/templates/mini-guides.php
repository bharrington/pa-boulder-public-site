<?php /*
Template Name: Mini Guides
*/
?>

<?php get_template_part( 'partials/global/_head' ); ?>

<section class="mini-guides">
  <nav>
    <ul class="breadcrumb">
      <li class="breadcrumb__item breadcrumb__item-current">Mini Guides</li>
    </ul>
  </nav>

  <article class="mini-guides__content">
    <p>Available mini guides to download. Select areas at the moment, in hopes of a more comprehensive guidebook in the future.</p>

    <div class="mini-guides__wrap">
      <div class="mini-guide">
        <a class="img-link" href="http://pabouldering.com/content/uploads/Sunfish-Pond-Main-v1.1.pdf" target="_blank" title="Sunfish Pond - Main Bouldering"><img src="<?php echo get_template_directory_uri() .'/assets/images/mini-guides/sfm-cv.jpg' ?>" /></a>
        <a href="http://pabouldering.com/content/uploads/Sunfish-Pond-Main-v1.1.pdf" class="btn" target="_blank">Get V1.1</a>
      </div>

      <div class="mini-guide">
        <a class="img-link" href="http://pabouldering.com/content/uploads/Sunfish-Pond-Southside-v1.1.pdf" target="_blank" title="Sunfish Pond - Southside Bouldering"><img src="<?php echo get_template_directory_uri() .'/assets/images/mini-guides/sfs-cv.jpg' ?>" /></a>
        <a href="http://pabouldering.com/content/uploads/Sunfish-Pond-Southside-v1.1.pdf" class="btn" target="_blank">Get V1.1</a>
      </div>

      <div class="mini-guide">
        <a class="img-link" href="http://pabouldering.com/content/uploads/Sunfish-Pond-Tower-v1.0.pdf" target="_blank" title="Sunfish Pond - Tower Bouldering"><img src="<?php echo get_template_directory_uri() .'/assets/images/mini-guides/sft-cv.jpg' ?>" /></a>
        <a href="http://pabouldering.com/content/uploads/Sunfish-Pond-Tower-v1.0.pdf" class="btn" target="_blank">Get V1.0</a>
      </div>

      <div class="mini-guide">
        <a class="img-link" href="http://pabouldering.com/content/uploads/Triple-Roofs-v1.0.pdf" target="_blank"title="Triple Roofs Bouldering"><img src="<?php echo get_template_directory_uri() .'/assets/images/mini-guides/tr-cv.jpg' ?>" /></a>
        <a href="http://pabouldering.com/content/uploads/Triple-Roofs-v1.0.pdf" class="btn" target="_blank">Get V1.0</a>
      </div>

      <div class="mini-guide">
        <a class="img-link" href="http://pabouldering.com/content/uploads/Carbon-Run-v1.0.pdf" target="_blank" title="Carbon Run Bouldering"><img src="<?php echo get_template_directory_uri() .'/assets/images/mini-guides/cr-cv.jpg' ?>" /></a>
        <a href="http://pabouldering.com/content/uploads/Carbon-Run-v1.0.pdf" class="btn" target="_blank">Get V1.0</a>
      </div>
    </div>

  </article>
</section>

<?php get_template_part( 'partials/global/_foot' ); ?>
