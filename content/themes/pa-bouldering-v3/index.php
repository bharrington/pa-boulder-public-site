<?php // path vars
$pa_global = 'partials/global/';
$pa_areas  = 'partials/page-types/areas/';
$pa_spray  = 'partials/page-types/spray/';
$pa_page   = 'partials/page-types/page/';
$pa_front  = 'partials/page-types/front-page/'; ?>

<?php get_template_part( $pa_global.'_head' ); ?>

<?php if ( is_singular( 'areas' ) ): ?>
  <div class="spray__item-pull"></div>
<?php endif; ?>

<?php if ( is_front_page() ): // home ?>
  <?php get_template_part( $pa_front.'_hero' ); ?>
  <?php get_template_part( $pa_front.'_areas-map' ); ?>
  <?php get_template_part( $pa_front.'_areas' ); ?>
  <?php get_template_part( $pa_front.'_latest-spray' ); ?>
  <?php get_template_part( $pa_front.'_popup' ); ?>
<?php endif; ?>


<?php if ( !is_singular( ) ): // spray archives ?>
  <?php get_template_part( $pa_spray.'_spray-listing' ); ?>
<?php endif; ?>


<?php if ( is_singular( 'post' ) ): // spray single ?>
  <?php get_template_part( $pa_spray.'_spray-single' ); ?>
<?php endif; ?>


<?php if ( is_page() && !is_front_page()): // pages ?>
  <?php get_template_part( $pa_page.'_page' ); ?>
<?php endif; ?>


<?php if ( is_singular( 'areas' ) ): // spray areas ?>
  <?php get_template_part( $pa_areas.'_areas-single' ); ?>
  <?php get_template_part( $pa_spray.'_spray-related' ); ?>
<?php endif; ?>


<?php if ( !is_singular( 'post' ) ): // footer ?>
  <?php get_template_part( $pa_global.'_foot' ); ?>
<?php endif; ?>
