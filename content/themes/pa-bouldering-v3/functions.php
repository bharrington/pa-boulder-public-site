<?php
require_once ('functions/theming/theme-assets-enqueue.php');      // Enqueue CSS and JS
require_once ('functions/basic/security.php');                    // Security helpers
require_once ('functions/basic/no-rss.php');                      // No RSS for static sites

// require_once ('functions/admin/editor-style.php');                // Load the site css as the editor style
require_once ('functions/admin/admin-panel.php');                 // Custom splash panel
require_once ('functions/admin/wp-admin-menu-classes.php');
require_once ('functions/admin/admin-functions.php');             // Custom functions and plugins
require_once ('functions/admin/admin-cleanup.php');               // Cleanup Admin Area
require_once ('functions/admin/no-theme-edit.php');               // Remove ability to edit themes via wp
require_once ('functions/admin/remove-meta.php');                 // Rid the meta tags menus, end users dont need this
require_once ('functions/admin/hide-admin-menu.php');             // Hide unused menu items

require_once ('functions/custom/custom-functions.php');           // Custom functions
require_once ('functions/custom/custom-fields.php');              // Custom Fields
require_once ('functions/custom/custom-posts.php');              // Custom Fields
require_once ('functions/custom/custom-image-sizes.php');         // Custom Image Sizes & Post Thumbnails

require_once ('functions/custom/custom-tinymce-base-edit.php');   // Cleans up tinymce
require_once ('functions/custom/custom-tinymce-items.php');       // Add additional tinymce menu items
?>
