$(".reveal-related-spray").click(function () {
  $('.reveal-related-spray').toggleClass('active');

  if ( $('.reveal-related-spray').hasClass('active') ) {
    $('.reveal-related-spray').html('&rarr;');
  } else {
    $('.reveal-related-spray').html('Related Spray');
  }

  var areaMapWidth = $('.areas__item-map').width();
  var calcMapWidth = function() { $('.areas__item--single').css('transform', 'translateX(-' + areaMapWidth + 'px)'); }

  if ( $('body').hasClass('show-related-spray') ) {
    $('.spray--listing-related').css('z-index', '-99');
    $('.areas__item--single').css('transform', 'translateX(0px)');
    $('.spray--listing-related').css('opacity', '0');

  } else {
    calcMapWidth();
    $('.spray--listing-related').css('opacity', '1');
    $('.spray--listing-related').css('z-index', '99');
  }

  $('body').toggleClass('show-related-spray');
});
