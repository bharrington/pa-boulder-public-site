function fetchSpray() {
  $.ajaxSetup({cache:false});

  $("a.pull-post").click(function(){
      var post_url = $(this).attr("href");
      var post_id = $(this).attr("rel");
      // $(".spray__item-pull").html('');
  $(".spray__item-pull").load(post_url);
  $('body').addClass('show-single-post');
  return false;
  });
}

$(document).ready(function(){
  fetchSpray();
});

$( document ).ajaxStop(function() {
  $('.breadcrumb__item-close').click(function() {
    $('body').removeClass('show-single-post');
    $(".spray__item-pull").empty();
    return false;
  });
});
