$( document ).ready(function() {
  // three column
  if ($('.three_column_content').length) {
    $('.three_column_content .column').matchHeight();
  }

  // 2 columns
  if ($('.two_column_content').length) {
    $('.two_column_content .column').matchHeight();
  }
});
