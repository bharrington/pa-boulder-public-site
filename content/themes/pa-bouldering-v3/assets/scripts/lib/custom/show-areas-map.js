$(document).ready(function(){
  // show
  $(".expand-areas__map").click(function(){
    initAreasMap();
    areasMapLoad();
    $('body').toggleClass('areas__map--expanded');
  });

  // center on click
  $(".expand-areas__map").click(function(e){
    var areasMap = $('#areas__map');
    var areasMapOffset = areasMap.offset().top;
    var areasMapHeight = areasMap.innerHeight();
    var windowHeight = $(window).height();
    var offset;

    if (areasMapHeight < windowHeight) {
      offset = areasMapOffset - ((windowHeight / 2) - (areasMapHeight));
    }
    else {
      offset = areasMapOffset;
    }

    var speed = 300;
    $('html, body').animate({scrollTop:offset}, speed);
  });
});
