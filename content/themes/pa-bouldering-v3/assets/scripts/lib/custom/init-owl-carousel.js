$( document ).ready(function() {
  if ($('.hero__item').length) {
    $(".hero__item").owlCarousel({
      navigation : false,
      pagination: true,
      lazyLoad : true,
      slideSpeed : 600,
      paginationSpeed : 400,
      singleItem : true,
      autoHeight : true
    });
  }

  if ($('.mini-slider.owl-carousel').length) {
    $(".mini-slider.owl-carousel").owlCarousel({
      items : 3,
      lazyLoad : true,
      navigation : false,
      slideSpeed : 600,
      paginationSpeed : 400,
      autoHeight : true
    });
  }

  if ($('.full-slider.owl-carousel').length) {
    $(".full-slider.owl-carousel").owlCarousel({
      navigation : false,
      pagination: false,
      lazyLoad : true,
      slideSpeed : 600,
      paginationSpeed : 400,
      singleItem : true,
      autoHeight : true
    });
  }
});
