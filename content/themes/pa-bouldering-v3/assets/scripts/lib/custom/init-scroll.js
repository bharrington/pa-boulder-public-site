$(document).ready(function() {
  var sprayScroller = function() {
    $('.spray--listing-related .scrollable').jScrollPane();
  }

  // init
  sprayScroller();

  // onresize
  var resizeSpray;
  window.onresize = function(){
    clearTimeout(resizeSpray);
    resizeSpray = setTimeout(sprayScroller, 100);
  };
});
