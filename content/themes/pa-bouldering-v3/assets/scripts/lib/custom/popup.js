$(document).ready(function(){
  var inst = $('[data-remodal-id=modal]').remodal();

  if (document.cookie.indexOf("pa-bouldering-popup") <= 0) {
     setTimeout(function(){
      $('[data-popup], [data-close-popup]').addClass('show');
    }, 2500);
  }

  $('[data-close-popup]').click(function() {
    $(this).remove();
    $('[data-popup]').remove();
    Cookies.set('name', 'pa-bouldering-popup', { expires: 7 });
  });

  $('[data-popup]').click(function() {
    inst.open();
    $(this).remove();
    $('[data-close-popup]').remove();
  });
});


