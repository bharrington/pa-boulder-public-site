## Building a module for styleguide

/*doc
---
title: Button
name: Button
category: Components - Button
---
```html_example
<a class='btn' href='http://google.com'>Fun Button</a>
<a class='btn btn-border' href='http://google.com'>Fun Button</a>
```
*/


## Styleguide
$ grunt connect
http://0.0.0.0:9001/styleguide/


# Writing Test via Casper
example:

casper.thenOpen('http://0.0.0.0:9001/styleguide/components_-_button.html')
  .then(function () {
    this.viewport(400, 1000);
      phantomcss.screenshot('.btn', 'element');
  })
  .then(function() {
    this.mouse.move('.btn');
    phantomcss.screenshot('.btn', 'element-hover');
  });


## Visual Testing
$ grunt connect - to start the styleguide server.
$ grunt _testClean - to clean baselines
$ grunt test - to run comparisons

Make changes and run $ grunt test --force to reevaluate.

You can target individual test.
$ grunt test --target="button-test"
