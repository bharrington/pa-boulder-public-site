<?php

// Dereg GF scripts
function deregister_scripts(){
  wp_deregister_script("jquery");
}

add_action("gform_enqueue_scripts", "deregister_scripts");


// Dereg WP jquerys
if( !is_admin()){
  wp_deregister_script('jquery');
  wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"), array(), '2.1.4',true);
  wp_enqueue_script('jquery');

  // wp_deregister_script('jquery-migrate');
  // wp_register_script('jquery-migrate', ("http://code.jquery.com/jquery-migrate-1.1.0.js"), array(), '1.1.0',true);
  // wp_enqueue_script('jquery-migrate');
}


// Load theme js & css
function theme_js_css() {
  wp_enqueue_style( 'style-name', get_template_directory_uri() . '/assets/styles/style.css' );
  wp_enqueue_script( 'application', get_template_directory_uri() . '/assets/scripts/application.min.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'theme_js_css' );


// Register icons
function theme_favicons() {
    echo '<link rel="shortcut icon" type="image/x-icon" href="'. get_template_directory_uri() .'/assets/images/icons/favicon.png" />';
    echo '<link rel="apple-touch-icon" href="'. get_template_directory_uri() .'/assets/images/icons/apple-touch-icon.png" />';
    echo '<link rel="apple-touch-icon" sizes="72x72" href="'. get_template_directory_uri() .'/assets/images/icons/apple-touch-icon-72x72.png" />';
    echo '<link rel="apple-touch-icon" sizes="114x114" href="'. get_template_directory_uri() .'/assets/images/icons/apple-touch-icon-114x114.png" />';
    echo '<link rel="apple-touch-icon" sizes="120x120" href="'. get_template_directory_uri() .'/assets/images/icons/apple-touch-icon-120x120.png" />';
    echo '<link rel="apple-touch-icon" sizes="144x144" href="'. get_template_directory_uri() .'/assets/images/icons/apple-touch-icon-144x144.png" />';
}

add_action('wp_head', 'theme_favicons');
