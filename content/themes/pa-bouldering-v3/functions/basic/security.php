<?php 
// remove version info from head and feeds
function complete_version_removal() {
    return '';
}

add_filter('the_generator', 'complete_version_removal');


//remove pings to self
function no_self_ping( &$links ) {
    $home = get_option( 'home' );
    foreach ( $links as $l => $link )
        if ( 0 === strpos( $link, $home ) )
            unset($links[$l]);
}

add_action( 'pre_ping', 'no_self_ping' );

// no error message on login failure
add_filter('login_errors',create_function('$a', "return null;"));
