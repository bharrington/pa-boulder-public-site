<?php
function set_infinite_scrolling(){ if(! is_singular()){ ?>
<script type="text/javascript">
    var inf_scrolling = {
        loading:{
            img: "<? echo get_template_directory_uri(); ?>/assets/images/ajax-loader.gif",
            msgText: "",
            finishedMsg: "C'mon Nah, that's all the spray son!",
        },

        "nextSelector":".navi a",
        "navSelector":".navi",
        "itemSelector":".all-spray .spray__item",
        "contentSelector":".all-spray"
    };

    jQuery(inf_scrolling.contentSelector).infinitescroll(inf_scrolling);
</script>

<? } } add_action( 'wp_footer', 'set_infinite_scrolling',100 ); ?>
