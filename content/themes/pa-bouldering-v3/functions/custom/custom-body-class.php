<?php
/*
 * remove defaulted body classes
 */
add_filter('body_class', 'my_body_class', 10, 2);
function my_body_class($wp_classes, $extra_classes) {
  // List of the only WP generated classes allowed
  $whitelist = array('home', 'page', 'page-parent', 'page-child', 'single-post', 'error404');

  // Filter the body classes
  $wp_classes = array_intersect($wp_classes, $whitelist);

  // Add the extra classes back untouched
  return array_merge($wp_classes, (array) $extra_classes);
}


/*
 * Add classes based on template name
 */
add_filter('body_class','custom_class_names');
function custom_class_names($c) {
  is_page_template('page-templates/example-template.php') ? $c[] = 'example-template' : null;

  return $c;
}

