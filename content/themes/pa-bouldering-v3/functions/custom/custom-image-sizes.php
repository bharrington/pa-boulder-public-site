<?php
add_action( 'after_setup_theme', 'setup' );
function setup() {
  add_image_size( 'mini-slider', 825, 619 );
  add_image_size( 'columns', 600, 450, true );
  add_image_size( 'hero', 1500, 9999 );
  add_image_size( 'hero-banner-l', 1500, 900, true );
  add_image_size( 'hero-banner-m', 900, 540, true );
  add_image_size( 'hero-banner-s', 600, 360, true );
}
