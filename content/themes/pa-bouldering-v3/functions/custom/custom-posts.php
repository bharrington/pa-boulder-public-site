<?php

// Create a custom post type
add_action('init', 'reg_areas');

function reg_areas() {
        $labels = array(
        'name' => 'Areas',
        'singular_name' => 'Area',
        'menu_name' => 'Areas',
        'add_new' => 'Add Area',
        'add_new_item' => 'Add New Area',
        'edit' => 'Edit',
        'edit_item' => 'Edit Area',
        'new_item' => 'New Area',
        'view' => 'View Area',
        'view_item' => 'View Area',
        'search_items' => 'Search Areas',
        'not_found' => 'No Areas Found',
        'not_found_in_trash' => 'No Areas Found in Trash',
        'parent' => 'Parent Area',
        );
        // Create an array for the $args
        $args = array( 'labels' => $labels, /* NOTICE: the $labels variable is used here... */
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,  // this sets a few defauls to true also, but they can be decalred separately if needed
                'query_var' => true,
                'rewrite' => true,
                'capability_type' => 'post',
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array( 'title', 'editor' ) // there are more if needed
        );

        register_post_type( 'areas', $args ); /* Register it and move on */

        // sometimes WP seems to messup on the permalinks as there are a few reported problems out there about the rewriting of custom URLs; in short, they sometimes don't work
        // so this should fix the issue. If not simply go to the Settings > Permalinks page, which will flush the permalinks (assuming that your WordPress install can write to the .htaccess file).
        flush_rewrite_rules();
}

?>
