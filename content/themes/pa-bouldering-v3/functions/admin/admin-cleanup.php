<?php

// Remove metabox
function remove_post_custom_fields() {
    remove_meta_box( 'postcustom' , 'page' , 'normal' ); 
}
add_action( 'admin_menu' , 'remove_post_custom_fields' );

// Remove Dashboard Metaboxes
add_action('admin_init', 'rw_remove_dashboard_widgets');
function rw_remove_dashboard_widgets() {
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   // right now
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // recent comments
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // incoming links
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // plugins
    remove_meta_box('dashboard_quick_press', 'dashboard', 'normal');  // quick press
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'normal');  // recent drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'normal');   // wordpress blog
    remove_meta_box('dashboard_secondary', 'dashboard', 'normal');   // other wordpress news
    remove_meta_box('dashboard_activity', 'dashboard', 'normal'); // dashboard activity
}

// Remove Smilies
function yst_remove_smileys($bool) {
  return false;
}
add_filter('option_use_smilies','yst_remove_smileys',99,1);


// Better SEO via TINYMce
function customformatTinyMCE($init) {
	// Add block format elements you want to show in dropdown
	$init['theme_advanced_blockformats'] = 'p,h3,h4';
	$init['theme_advanced_disable'] = 'strikethrough,underline,pre,h1,help,separator,numlist,forecolor,justifyfull';
	return $init;
}

// Modify Tiny_MCE init
add_filter('tiny_mce_before_init', 'customformatTinyMCE' );

// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
add_action('init', 'remheadlink');
function remheadlink() {
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
}

add_filter( 'next_post_rel_link', 'disable_stuff' );

function disable_stuff( $data ) {
	return false;
}

// Remove Widgets
function unregister_default_wp_widgets() {
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
}

add_action('widgets_init', 'unregister_default_wp_widgets', 1);

?>