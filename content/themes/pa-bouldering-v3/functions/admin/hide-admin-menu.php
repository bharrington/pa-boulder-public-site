<?php
add_action('admin_menu','my_admin_menu');
function my_admin_menu() {
  rename_admin_menu_section('Posts','Spray');
  remove_admin_menu_section('edit-comments.php');
}

function custom_menu_order($menu_ord) {
  if (!$menu_ord) return true;

  return array(
    'index.php', // Dashboard
    'separator1', // First separator
    'edit.php', // Posts
    'edit.php?post_type=areas',
    'history',
    'hero',
    'separator2', // First separator
    'upload.php', // Media
  );
}

add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');

add_action( 'admin_head', 'admin_css_2' );
function admin_css_2(){ ?>
  <style>
    #adminmenu #toplevel_page_history .dashicons-before:before,
    #adminmenu #toplevel_page_hero .dashicons-before:before {
      content: "\f109" !important;
    }
  </style>
<?php
}
