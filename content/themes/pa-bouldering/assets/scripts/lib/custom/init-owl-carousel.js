$( document ).ready(function() {
  if ($('.row .owl-carousel').length) {
    $(".owl-carousel").owlCarousel({
      navigation : true,
      slideSpeed : 600,
      paginationSpeed : 400,
      singleItem : true,
      autoHeight : true
    });
  }
});
