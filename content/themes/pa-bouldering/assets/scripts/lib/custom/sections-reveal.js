$(document).ready(function() {
  var menu_mini_guides = $( '.menu__mini_guides' ),
      menu_areas       = $( '.menu__areas' ),
      menu_history     = $( '.menu__history' ),
      menu_spray       = $( '.menu__spray' ),
      menu_gossip      = $( '.menu__gossip' ),
      latest_spray     = $( '.latest-spray' ),
      logo             = $( '.logo' ),
      menu_fa          = $( '.menu__fa' ),
      vid              = document.getElementById("bgvid"),
      body             = document.querySelector( 'body' );

  function revealGuide(ev) {
    if( classie.has( body, '_show-menu' ) ) {
      classie.remove( body, '_show-menu');
      classie.add( body, '_show-mini-guides');
      ev.preventDefault();
    }
  }

  function revealArea(ev) {
    if( classie.has( body, '_show-menu' ) ) {
      classie.remove( body, '_show-menu');
      classie.add( body, '_show-areas');
      matchHeight_boxed();
      ev.preventDefault();
    }
  }

  function revealGossip(ev) {
    if( classie.has( body, '_show-menu' ) ) {
      classie.remove( body, '_show-menu');
      classie.add( body, '_show-gossip');
      ev.preventDefault();
    }
  }

  function revealHistory(ev) {
    if( classie.has( body, '_show-menu' ) ) {
      classie.remove( body, '_show-menu');
      classie.add( body, '_show-history');
      ev.preventDefault();
    }
  }

  function revealSpray(ev) {
    if( classie.has( body, '_show-menu' ) ) {
      classie.remove( body, '_show-menu');
      classie.add( body, '_show-spray');
      ev.preventDefault();
    }
  }

  function revealLatestSpray(ev) {
    classie.add( body, '_blur-bg');
    classie.add( body, '_show-spray');
    vid.pause();
    ev.preventDefault();
  }

  function revealFa(ev) {
    if( classie.has( body, '_show-menu' ) ) {
      classie.remove( body, '_show-menu');
      classie.add( body, '_show-fa');
      ev.preventDefault();
    }
  }

  menu_areas.click( revealArea );
  menu_history.click( revealHistory );
  menu_spray.click( revealSpray );
  // menu_gossip.click( revealGossip );
  menu_mini_guides.click( revealGuide );
  latest_spray.click( revealLatestSpray );
  logo.click( revealLatestSpray );
  // menu_fa.click( revealFa );
});
