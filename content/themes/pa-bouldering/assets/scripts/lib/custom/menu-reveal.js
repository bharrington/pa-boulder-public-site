$(document).ready(function() {
  var menuRevealer = $( '.menu__trigger' ),
      body = document.querySelector( 'body' ),
      section = $( '.area__single--detail');
      // vid = document.getElementById("bgvid");

  function hideProblems(ev) {
    $(section).removeClass('_show-problems');
    ev.preventDefault();
  }

  function revealMenu(ev) {
    if ( classie.has( body, '_show-problems' ) ) {
      classie.remove( body, '_show-problems');
      hideProblems();
      ev.preventDefault();

    } else if ( classie.has( body, '_show-menu' ) ) {
      classie.remove( body, '_show-menu');
      classie.remove( body, '_blur-bg');

      // home page video
      // if ($('.video-controls.playing').length) {
      //   vid.play();
      // }

      ev.preventDefault();

    } else if ( !classie.has( body, '_show-menu' ) ) {
      classie.add( body, '_show-menu');
      classie.add( body, '_blur-bg');
      classie.remove( body, '_show-areas');
      classie.remove( body, '_show-history');
      classie.remove( body, '_show-app');
      classie.remove( body, '_show-spray');
      classie.remove( body, '_show-fa');
      classie.remove( body, '_show-gossip');
      classie.remove( body, '_show-mini-guides');

      // home page video
      // vid.pause();
      ev.preventDefault();
    }
  }

  menuRevealer.click( revealMenu );
});
