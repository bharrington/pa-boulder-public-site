<?php

function remove_metaboxes() {
 remove_meta_box( 'postcustom' , 'page' , 'normal' ); //removes custom fields for page
 remove_meta_box( 'commentstatusdiv' , 'page' , 'normal' ); //removes comments status for page
 remove_meta_box( 'commentsdiv' , 'page' , 'normal' ); //removes comments for page
 remove_meta_box( 'authordiv' , 'page' , 'normal' ); //removes author for page
}
add_action( 'admin_menu' , 'remove_metaboxes' );
