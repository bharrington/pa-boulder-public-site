<?php // Admin Functions
function pab2_custom_css() {
  echo '<style type="text/css">

        .add-space {
          padding: 15px !important;
        }

        .pab-admin-area-map {
          float: left !important;
          margin-right: 2% !important;
        }

        .pab-admin-area-map:first-child {
          margin-right: 2% !important;
        }

        .pab-admin-area-map:last-child {
          margin-right: 0% !important;
          margin-left: 2% !important;
        }

        </style>';
  }

add_action('admin_head', 'pab2_custom_css');
