<?php
// Additional WP sizes
add_action( 'after_setup_theme', 'setup' );
function setup() {
  add_image_size( 'mini-slider', 825, 619 );
  add_image_size( 'columns', 600, 450, true );
  add_image_size( 'banner', 1400, 900, true );
}
