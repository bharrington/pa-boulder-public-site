<?php get_template_part( 'partials/common/_head' ); ?>
<?php get_template_part( 'partials/common/_header' ); ?>
<?php get_template_part( 'partials/page-types/front-page/_latest-spray' ); ?>

<?php get_template_part( 'partials/common/navigation/_main' ); ?>
<?php get_template_part( 'partials/page-types/front-page/_hero' ); ?>
<?php get_template_part( 'partials/page-types/front-page/_mini-guides' ); ?>
<?php get_template_part( 'partials/page-types/front-page/_areas' ); ?>
<?php get_template_part( 'partials/page-types/front-page/_history' ); ?>
<?php get_template_part( 'partials/page-types/front-page/_app' ); ?>
<?php get_template_part( 'partials/page-types/front-page/_spray' ); ?>
<?php get_template_part( 'partials/page-types/front-page/_fa' ); ?>

<?php get_template_part( 'partials/common/_foot' ); ?>
