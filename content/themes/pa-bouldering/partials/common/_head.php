<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
	<meta charset="utf-8">
	<title><?php bloginfo('name'); ?> - <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <?php wp_head(); ?>

</head>
<body <?php body_class(); ?> >