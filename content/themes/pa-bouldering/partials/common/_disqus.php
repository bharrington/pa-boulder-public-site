<?php $disqus = get_field('disqus_id', 'options'); ?>

  <?php if( $disqus ): ?>
    <div class="disqus" id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = '<?php echo $disqus ?>';

        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
  <?php endif; ?>
