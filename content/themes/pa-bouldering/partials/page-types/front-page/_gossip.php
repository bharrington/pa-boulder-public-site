<section class="gossip">
  <aside>
    <h2>Gossip</h2>
  </aside>

  <article>
    <?php if( get_field('gossip_content', 'option') ): ?>
      <?php the_field('gossip_content', 'option'); ?>
    <?php endif;  ?>

    <?php get_template_part( 'partials/common/_disqus' ); ?>
  </article>
</section>
