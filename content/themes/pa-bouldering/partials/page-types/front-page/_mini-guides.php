<section class="mini-guides">
  <aside>
    <h2>Mini Guides</h2>
    <?php the_field('mini_guide_content', 'option'); ?>
  </aside>

  <article>
  <?php
    $areas_args = array(
      'posts_per_page' => 9999,
      'post_type' => 'Areas',
      'orderby' => 'title',
      'order' => 'ASC',
    );

    $areas = new WP_Query( $areas_args );
  ?>

    <?php while ($areas->have_posts()) : $areas->the_post(); ?>
      <div class="mini-guides listing">
        <?php if( get_field('area_link_to_guide') ): ?>
          <div class="mini-guide-wrapper">
            <a class="btn" href="<?php the_field('area_link_to_guide'); ?>" target="_blank">
              <?php the_field('guide_name'); ?> <?php the_field('guide_version'); ?>
            </a>
          </div>
        <?php endif;  ?>

        <?php if( get_field('link_to_guide_2') ): ?>
          <div class="mini-guide-wrapper">
            <a class="btn" href="<?php the_field('link_to_guide_2'); ?>" target="_blank">
              <?php the_field('guide_name_2'); ?> <?php the_field('guide_version_2'); ?>
            </a>
          </div>
        <?php endif;  ?>

        <?php if( get_field('link_to_guide_3') ): ?>
          <div class="mini-guide-wrapper">
            <a class="btn" href="<?php the_field('link_to_guide_3'); ?>" target="_blank">
              <?php the_field('guide_name_3'); ?> <?php the_field('guide_version_3'); ?>
            </a>
          </div>
        <?php endif;  ?>

        <?php if( get_field('link_to_guide_4') ): ?>
          <div class="mini-guide-wrapper">
            <a class="btn" href="<?php the_field('link_to_guide_4'); ?>" target="_blank">
              <?php the_field('guide_name_4'); ?> <?php the_field('guide_version_4'); ?>
            </a>
          </div>
        <?php endif;  ?>
      </div>
    <?php endwhile; ?><?php wp_reset_query(); ?>
  </article>
</section>
