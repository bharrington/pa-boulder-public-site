<section class="history">
  <aside>
    <h2>History</h2>
    <?php the_field('history_content', 'option'); ?>
  </aside>

  <?php get_template_part( 'partials/page-types/front-page/history/_content' ); ?>
</section>
