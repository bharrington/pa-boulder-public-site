<?php
  $areas_args = array(
    'posts_per_page' => 9999,
    'post_type' => 'Areas',
  );

  $areas = new WP_Query( $areas_args );
?>

<?php while ($areas->have_posts()) : $areas->the_post(); ?>
<section id="<?php  global $post; $post_slug=$post->post_name; echo $post_slug; ?>" class="area__single--detail">
  <aside>
    <h2><?php the_title(); ?></h2>

    <?php if( get_field('area_closest_town') ): ?>
      <p class="closest-town"><b><?php the_field("area_closest_town"); ?></b></p>
    <?php endif;  ?>

    <?php the_content(); ?>

    <?php if( get_field('area_link_to_guide') ): ?>
      <p class="link-to-guide">
        <a class="btn" href="<?php the_field('area_link_to_guide'); ?>" target="_blank">
          <?php the_field('guide_name'); ?> <?php the_field('guide_version'); ?>
        </a>
      </p>
    <?php endif;  ?>

    <?php if( get_field('link_to_guide_2') ): ?>
      <p class="link-to-guide">
        <a class="btn" href="<?php the_field('link_to_guide_2'); ?>" target="_blank">
          <?php the_field('guide_name_2'); ?> <?php the_field('guide_version_2'); ?>
        </a>
      </p>
    <?php endif;  ?>

    <?php if( get_field('link_to_guide_3') ): ?>
      <p class="link-to-guide">
        <a class="btn" href="<?php the_field('link_to_guide_3'); ?>" target="_blank">
          <?php the_field('guide_name_3'); ?> <?php the_field('guide_version_3'); ?>
        </a>
      </p>
    <?php endif;  ?>

    <?php if( get_field('link_to_guide_4') ): ?>
      <p class="link-to-guide">
        <a class="btn" href="<?php the_field('link_to_guide_4'); ?>" target="_blank">
          <?php the_field('guide_name_4'); ?> <?php the_field('guide_version_4'); ?>
        </a>
      </p>
    <?php endif;  ?>
  </aside>

  <article class="area-map">
    <?php the_field('area_map'); ?>
  </article>

</section>
<?php endwhile; ?><?php wp_reset_query(); ?>

