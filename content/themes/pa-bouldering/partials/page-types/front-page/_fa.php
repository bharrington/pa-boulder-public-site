<section class="fa">
  <aside>
    <h2>First Ascent?</h2>
    <?php get_template_part( 'partials/common/_disqus' ); ?>
  </aside>

  <?php if( get_field('first_ascent_content', 'option') ): ?>
  <article>
    <?php the_field('first_ascent_content', 'option'); ?>
  </article>
  <?php endif;  ?>
</section>
