<?php
  $hero_banner    = get_field('home_banner', 'option');
  $banner_resized = wp_get_attachment_image_src($hero_banner, 'banner');
?>

<?php if( $banner_resized ): ?>
  <section class="hero" style="background-image: url(<?php echo $banner_resized[0]; ?>);"></section>
<?php else: ?>
  <section class="hero"></section>
<?php endif;  ?>
