<section class="areas">
  <aside>
    <h2>Areas</h2>
    <?php the_field('areas_content', 'options'); ?>
  </aside>


<?php
  $areas_args = array(
    'posts_per_page' => 9999,
    'post_type' => 'Areas',
    'meta_key' => 'area_rating',
    'orderby'   => 'meta_value',
    'order'     => 'DESC',
  );

  $areas = new WP_Query( $areas_args );
?>

  <article>
    <ul class="_not-list">
      <?php while ($areas->have_posts()) : $areas->the_post(); ?>
      <li class="areas__single boxed">
        <a class="<?php  global $post; $post_slug=$post->post_name; echo $post_slug; ?>" href="#">
          <div class="boxed__container">
            <h3><?php the_title(); ?></h3>

            <?php if( get_field('area_rating') ): ?>
              <p class="rating"><?php the_field('area_rating'); ?></p>
            <?php endif; ?>

            <?php if( get_field('area_problem_count') ): ?>
              <p class="problem-count"><?php the_field('area_problem_count'); ?></p>
            <?php endif; ?>
          </div>
        </a>
      </li>
    <?php endwhile; ?><?php wp_reset_query(); ?>
    </ul>
  </article>
</section>

<?php get_template_part( 'partials/page-types/front-page/_areas-single' ); ?>
