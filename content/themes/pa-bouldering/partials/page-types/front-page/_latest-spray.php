<?php
  $latest_spray = array(
    'posts_per_page' => 1,
    'post_type' => 'post',
  );

  $spray = new WP_Query( $latest_spray );
  while ($spray->have_posts()) : $spray->the_post();
?>

<div class="latest-spray">
  <div class="inner">
    <h3><?php the_title(); ?></h3>
    <span class="date">POSTED: <?php the_date('M d, Y'); ?></span>
    <span class="author">BY: <?php the_author(); ?></span>
  </div>
</div>

<?php endwhile; ?><?php wp_reset_query(); ?>
