<?php if( get_field('flexible_content_post') ):
  while ( has_sub_field('flexible_content_post') ) :
    $rowName = get_row_layout();
    echo '<div class="row '.$rowName.'">';
    include('layouts/'.$rowName.'.php');
    echo '</div>';
  endwhile;
endif;?>
