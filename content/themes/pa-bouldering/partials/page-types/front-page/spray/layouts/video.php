<div class="video-wrapper">
  <div class="video-embed">
    <?php the_sub_field('video'); ?>
  </div>
</div>

<?php if( get_sub_field('video_text') ): ?>
  <p class="small-text"><?php the_sub_field('video_text'); ?></p>
<?php endif; ?>
