<article>
  <?php if( get_field('flexible_content_history', 'options') ):
    while ( has_sub_field('flexible_content_history', 'options') ) :
      $rowName = get_row_layout();
      echo '<div class="row '.$rowName.'">';
      include('layouts/'.$rowName.'.php');
      echo '</div>';
    endwhile;
  endif;?>
</article>
