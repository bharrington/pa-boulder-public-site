<section class="spray">
  <aside>
    <h2>Spray</h2>
    <?php the_field('spray_content', 'option'); ?>
  </aside>

  <article>
    <div class="all-spray">
    <?php get_posts(); ?>
      <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <div class="spray__item">
        <h3><?php the_title(); ?></h3>
        <span class="date">POSTED: <?php the_date('M d, Y'); ?></span>   <span class="author">BY: <?php the_author(); ?></span>
        <?php get_template_part( 'partials/page-types/front-page/spray/_content' ); ?>
      </div>
    <?php endwhile; wp_reset_query(); ?>
  </div>
    <div class="navi">
      <?php next_posts_link( 'Gimme dat spray...' ); ?>
    </div>
  </article>
</section>
