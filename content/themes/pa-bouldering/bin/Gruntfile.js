module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // paths
    meta: {
      assetPath_style: '../assets/styles/',
      assetPath_style_blessed: 'a../ssets/styles/ie/',
      assetPath_script: '../assets/scripts/',
      assetPath_script_lib: '../assets/scripts/lib/',
      assetPath_script_depend: '../assets/scripts/lib/dependencies/',
      assetPath_script_vendor: '../assets/scripts/lib/vendor/',
      assetPath_script_custom: '../assets/scripts/lib/custom',
      assetPath_image: '../assets/images/',
    },

    // sass via compass
    compass: {
      dist: {
        options: {
          sassDir: '<%= meta.assetPath_style %>/scss/',
          cssDir: '<%= meta.assetPath_style %>/',
          environment: 'production',
          bundleExec: true,
          config: 'config.rb'
        }
      }
    },

    // concat js
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['<%= meta.assetPath_script_depend %>/*.js' , '<%= meta.assetPath_script_vendor %>/*.js' , '<%= meta.assetPath_script_custom %>/*.js'],
        dest: '<%= meta.assetPath_script %>/application.min.js'
      }
    },

    // uglify js
    uglify: {
      options: {
        banner: ''
      },
      dist: {
        files: {
          '<%= meta.assetPath_script %>/application.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },

    // optimize images
    imagemin: {
      png: {
        options: {
          optimizationLevel: 7
        },
        files: [
          {
            expand: true,
            cwd: '<%= meta.assetPath_image %>',
            src: ['**/*.png'],
            dest: '<%= meta.assetPath_image %>',
            ext: '.png'
          }
        ]
      },
      jpg: {
        options: {
          progressive: true
        },
        files: [
          {
            expand: true,
            cwd: '<%= meta.assetPath_image %>',
            src: ['**/*.jpg'],
            dest: '<%= meta.assetPath_image %>',
            ext: '.jpg'
          }
        ]
      }
    },

    // run browsersync
    browserSync: {
      dev: {
        bsFiles: {
          src : '<%= meta.assetPath_style %>/*.css'
        },
        options: {
          watchTask: true,
          host: "192.168.1.1",
          proxy: 'local.dev'
        },
        ghostMode: {
          clicks: true,
          scroll: true,
          links: true,
          forms: true
        }
      }
    },

    // watch files
    watch: {
      scripts: {
        files: [
          '<%= meta.assetPath_script_lib %>/**/*.js'
        ],
        tasks: ['concat', 'uglify'],
        options: {
          livereload: true,
        },
      },
      css: {
        files: [
          '<%= meta.assetPath_style %>/scss/**/*.scss'
        ],
        tasks: ['compass'],
        options: {
          livereload: true,
        },
      },
      images: {
        files: [
          '<%= meta.assetPath_image %>/**/*.*'
        ],
        tasks: ['imagemin'],
        options: {
          spawn: false,
        }
      }
    }

  });

  // plugins
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  // tasks
  grunt.registerTask('crush', ['imagemin']);
  grunt.registerTask('browser', ['browserSync']);
};
