<?php
// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . '/env_local' ) ) {

  // Local Environment
  // Add a file called env_local to the root
  define('WP_ENV', 'local');
  define('WP_DEBUG', false);
  define('DB_NAME', 'pa_bouldering_v2');
  define('DB_USER', 'root');
  define('DB_PASSWORD', 'root');
  define('DB_HOST', 'localhost');

} elseif ( file_exists( dirname( __FILE__ ) . '/env_staging' ) ) {

	// Stagin Environment
  // Add a file called env_staging to the root of the staging server
	define('WP_ENV', 'staging');
  define('WP_DEBUG', false);
  define('DB_NAME', 'stage_db');
  define('DB_USER', 'user');
  define('DB_PASSWORD', 'password');
  define('DB_HOST', 'localhost');

} else {

  // Production Environment
  // Be sure to have neither of the above suggestions in the root of the production server
  define('WP_ENV', 'production');
  define('WP_DEBUG', false);
  define('DB_NAME', 'brian_pabouldering_2014');
  define('DB_USER', 'brian_paboulder');
  define('DB_PASSWORD', 'cl!mb!2');
  define('DB_HOST', 'localhost');
  define('DB_CHARSET', 'utf8');
}

// ========================
// Custom Content Directory
// ========================
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/content' );
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/content' );

// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );

// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// ==============================================================
$table_prefix  = 'wp_';

// ================================
// Language
// Leave blank for American English
// ================================
define( 'WPLANG', '' );

// ===========
// Hide errors
// ===========
ini_set( 'display_errors', 0 );
define( 'WP_DEBUG_DISPLAY', false );

// =================================================================
// Debug mode
// Debugging? Enable these.
// =================================================================
// define( 'SAVEQUERIES', true );
// define( 'WP_DEBUG', true );

// ===================
// Bootstrap WordPress
// ===================
if ( !defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );
require_once( ABSPATH . 'wp-settings.php' );
